package no.noroff.MovieCharacterAPI.controllers;

import no.noroff.MovieCharacterAPI.models.Character;
import no.noroff.MovieCharacterAPI.models.Franchise;
import no.noroff.MovieCharacterAPI.models.Movie;
import no.noroff.MovieCharacterAPI.repositories.CharacterRepository;
import no.noroff.MovieCharacterAPI.repositories.FranchiseRepository;
import no.noroff.MovieCharacterAPI.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/franchises")
public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CharacterRepository characterRepository;

    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id){
        Franchise retFranchise = new Franchise();
        HttpStatus status;
        // We first check if the Library exists, this saves some computing time.
        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            retFranchise = franchiseRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(retFranchise, status);
    }

    @GetMapping("/{id}/movies")
    public ResponseEntity<List<Movie>> getMoviesForFranchise(@PathVariable Long id) {
        Franchise franchise;
        List<Movie> movies = new ArrayList<>();
        HttpStatus status;
        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            franchise = franchiseRepository.findById(id).get();
            movies = movieRepository.findAll().stream().filter(movie -> movie.getFranchise() == franchise).collect(Collectors.toList());
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(movies, status);
    }

    @GetMapping("/{id}/characters")
    public ResponseEntity<List<Character>> getCharactersForFranchise(@PathVariable Long id) {
        Franchise franchise;
        List<Character> characters = new ArrayList<>();
        HttpStatus status;
        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            franchise = franchiseRepository.findById(id).get();
            characters = characterRepository.findAll()
                    .stream()
                    .filter(character -> character
                            .getMovies()
                            .stream()
                            .anyMatch(movie -> movie.getFranchise()==franchise))
                            .collect(Collectors.toList());

        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(characters, status);
    }

    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        Franchise retFranchise = franchiseRepository.save(franchise);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(retFranchise, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise){
        Franchise retFranchise = new Franchise();
        HttpStatus status;
        /*
         We want to check if the request body matches what we see in the path variable.
         This is to ensure some level of security, making sure someone
         hasn't done some malicious stuff to our body.
        */
        if(!id.equals(franchise.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(retFranchise,status);
        }
        retFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(retFranchise, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFranchise(@PathVariable Long id){
        HttpStatus status;
        // We first check if the Library exists, this saves some computing time.
        if(franchiseRepository.existsById(id)){
            status = HttpStatus.NO_CONTENT;
            franchiseRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }
}

{
	"info": {
		"_postman_id": "b0c86db2-7879-4121-afcd-f2dd1afbc97b",
		"name": "Movie Character API Collection",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
	},
	"item": [
		{
			"name": "GET all characters",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/characters",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"characters"
					]
				},
				"description": "Gets all characters"
			},
			"response": []
		},
		{
			"name": "GET all movies",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/movies",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"movies"
					]
				},
				"description": "Get all movies\r\n"
			},
			"response": []
		},
		{
			"name": "GET all franchises",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/franchises",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"franchises"
					]
				}
			},
			"response": []
		},
		{
			"name": "POST a character",
			"request": {
				"method": "POST",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"fullName\": \"Name Lastname\",\r\n    \"alias\": \"Fireball\",\r\n    \"gender\": \"f\",\r\n    \"picture\": \"firesprite.png\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/characters",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"characters"
					]
				},
				"description": "Adds a new character"
			},
			"response": []
		},
		{
			"name": "POST a movie",
			"request": {
				"method": "POST",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "    {\r\n        \"title\": \"Movie: The Musical\",\r\n        \"genre\": \"musical\",\r\n        \"releaseYear\": 2003,\r\n        \"director\": \"Director\",\r\n        \"picture\": \"musical.jpg\",\r\n        \"trailer\": \"musical.mp4\",\r\n        \"characters\": [{\r\n            \"id\": 1\r\n        }]\r\n    }",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/movies",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"movies"
					]
				},
				"description": "Adds a movie."
			},
			"response": []
		},
		{
			"name": "POST a franchise",
			"request": {
				"method": "POST",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"name\": \"Movie Series\",\r\n    \"description\": \"The Movie series of movies\",\r\n    \"movies\": [\r\n        {\"id\": 2},\r\n        {\"id\": 1}\r\n    ]\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/franchises",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"franchises"
					]
				},
				"description": "Adds a franchise"
			},
			"response": []
		},
		{
			"name": "PUT a character by ID",
			"request": {
				"method": "PUT",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"id\": 1,\r\n    \"fullName\": \"Full Name\",\r\n    \"alias\": \"Alias\",\r\n    \"gender\": \"f\",\r\n    \"picture\": \"picture.png\",\r\n    \"movies\": [{\"id\":1}]\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/characters/1",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"characters",
						"1"
					]
				}
			},
			"response": []
		},
		{
			"name": "PUT a movie by ID",
			"request": {
				"method": "PUT",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n        \"id\": 2,\r\n        \"title\": \"A Movie Title: the Sequel\",\r\n        \"genre\": \"comedy\",\r\n        \"releaseYear\": \"1970-01-01T00:00:02.021+00:00\",\r\n        \"director\": \"Director\",\r\n        \"picture\": \"movie2.jpg\",\r\n        \"trailer\": \"Trailer2.mp4\"\r\n    }",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/movies/2",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"movies",
						"2"
					]
				},
				"description": "Updates movie id: 2"
			},
			"response": []
		},
		{
			"name": "PUT a franchise by ID",
			"request": {
				"method": "PUT",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"id\": 1,\r\n    \"name\": \"A Movie Series\",\r\n    \"description\": \"A Movie Series of the movie series\",\r\n    \"movies\": \r\n        [\r\n        {\"id\": 1},\r\n        {\"id\": 2}\r\n        ]\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/franchises/1",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"franchises",
						"1"
					]
				},
				"description": "Updates franchise id: 1"
			},
			"response": []
		},
		{
			"name": "GET character by ID",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/characters/1",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"characters",
						"1"
					]
				}
			},
			"response": []
		},
		{
			"name": "GET all characters from movie by movie ID",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/movies/1/characters",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"movies",
						"1",
						"characters"
					]
				},
				"description": "Gets all characters in movie id: 3"
			},
			"response": []
		},
		{
			"name": "GET movie by ID",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/movies/1",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"movies",
						"1"
					]
				},
				"description": "Get movie id: 3"
			},
			"response": []
		},
		{
			"name": "GET a franchise by ID",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/franchises/1",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"franchises",
						"1"
					]
				}
			},
			"response": []
		},
		{
			"name": "DELETE character by ID",
			"request": {
				"method": "DELETE",
				"header": [],
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/characters/1",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"characters",
						"1"
					]
				},
				"description": "Deletes character id: 2"
			},
			"response": []
		},
		{
			"name": "DELETE movie by ID",
			"request": {
				"method": "DELETE",
				"header": [],
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/movies/2",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"movies",
						"2"
					]
				}
			},
			"response": []
		},
		{
			"name": "DELETE franchise by ID",
			"request": {
				"method": "DELETE",
				"header": [],
				"url": {
					"raw": "https://moviecharacterapi.herokuapp.com/api/v1/franchises/1",
					"protocol": "https",
					"host": [
						"moviecharacterapi",
						"herokuapp",
						"com"
					],
					"path": [
						"api",
						"v1",
						"franchises",
						"1"
					]
				}
			},
			"response": []
		}
	],
	"protocolProfileBehavior": {}
}